import { ArgsType, Field, Float } from '@nestjs/graphql';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsEnum,
  IsNumber,
  IsOptional,
  MaxLength,
} from 'class-validator';

import { PaginationArgs, SortDirection } from '../../shared/dto';
import { CarsSortField } from './cars-sort-field.enum';

@ArgsType()
export class GetCarsArgs extends PaginationArgs {
  @Field({ nullable: true })
  @MaxLength(100)
  @IsOptional()
  model?: string;

  @Field(() => [Float], { nullable: 'itemsAndList' })
  @IsNumber({}, { each: true })
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsOptional()
  price?: [number | null, number | null];

  @Field(() => CarsSortField, { nullable: true })
  @IsEnum(CarsSortField)
  @IsOptional()
  sortBy?: CarsSortField;

  @Field(() => SortDirection, { nullable: true })
  @IsEnum(SortDirection)
  @IsOptional()
  sortDirection?: SortDirection;
}
