import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { CarsService } from './cars.service';
import { CarsResolver } from './cars.resolver';
import { CarsRepository } from './cars.repository';
import { CarModel, CarSchema } from './models';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: CarModel.name, schema: CarSchema }]),
  ],
  providers: [CarsResolver, CarsService, CarsRepository],
})
export class CarsModule {}
