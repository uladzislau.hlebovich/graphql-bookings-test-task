import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CarDocument = CarModel & Document;

@Schema({ collection: 'cars' })
export class CarModel {
  @Prop()
  model!: string;

  @Prop()
  pricePerHour!: number;
}

export const CarSchema = SchemaFactory.createForClass(CarModel);
