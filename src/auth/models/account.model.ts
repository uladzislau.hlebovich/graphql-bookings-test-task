import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type AccountDocument = AccountModel & Document;

@Schema({ collection: 'accounts' })
export class AccountModel {
  @Prop()
  username!: string;

  @Prop()
  hashedPassword!: string;
}

export const AccountSchema = SchemaFactory.createForClass(AccountModel);
