import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { AccountDocument, AccountModel } from './models';

type CreateProps = Pick<AccountDocument, 'username' | 'hashedPassword'>;

@Injectable()
export class AccountRepository {
  constructor(
    @InjectModel(AccountModel.name)
    private readonly accountModel: Model<AccountDocument>,
  ) {}

  create(props: CreateProps): Promise<AccountDocument> {
    const account = new this.accountModel(props);

    return account.save();
  }

  findByUsername(username: string): Promise<AccountDocument | null> {
    return this.accountModel.findOne({ username }).exec();
  }
}
