import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { AccountModel } from './models';

export const CURRENT_USERNAME_KEY = 'currentUsername';

@Injectable({ scope: Scope.REQUEST })
export class WebSessionService {
  constructor(
    @Inject(REQUEST)
    private readonly request: Request,
  ) {}

  storeAccount(account: AccountModel): void {
    this.request.session ??= {};
    this.request.session[CURRENT_USERNAME_KEY] = account.username;
  }

  getStoredUsername(): string | null {
    return this.request.session?.[CURRENT_USERNAME_KEY] ?? null;
  }

  hasStoredAccount(): boolean {
    return !!this.request.session?.[CURRENT_USERNAME_KEY];
  }

  destroy(): void {
    this.request.session = null;
  }
}
