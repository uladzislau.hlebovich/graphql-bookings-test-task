import { NestFactory } from '@nestjs/core';
import cookieSession from 'cookie-session';

import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const configService = app.get(ConfigService);

  app.use(
    cookieSession({
      name: 'PHPSESSID',
      sameSite: 'strict',
      keys: [configService.get('COOKIE_SECRET') as string],
      maxAge: 7 * 24 * 60 * 60 * 1000, // 7 days
    }),
  );

  await app.listen(3000);
}
bootstrap();
