import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type OfficeDocument = OfficeModel & Document;

@Schema({ collection: 'offices' })
export class OfficeModel {
  @Prop()
  address!: string;

  @Prop()
  postalCode!: string;

  @Prop()
  pricePerHour!: number;
}

export const OfficeSchema = SchemaFactory.createForClass(OfficeModel);
