import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, QueryOptions } from 'mongoose';

import { OfficeDocument, OfficeModel } from './models';
import { GetOfficesArgs } from './dto';
import { SortDirection } from '../shared/dto';

type OfficesFilterType = Pick<GetOfficesArgs, 'address' | 'price'>;
type OfficesOptionsType = Pick<
  GetOfficesArgs,
  'skip' | 'take' | 'sortBy' | 'sortDirection'
>;

@Injectable()
export class OfficesRepository {
  constructor(
    @InjectModel(OfficeModel.name)
    private readonly officeModel: Model<OfficeDocument>,
  ) {}

  async findAll(
    args: GetOfficesArgs = new GetOfficesArgs(),
  ): Promise<OfficeModel[]> {
    return this.officeModel
      .find(this.buildFilter(args), {}, this.buildOptions(args))
      .exec();
  }

  private buildFilter(args: OfficesFilterType): FilterQuery<OfficeDocument> {
    const f = {} as FilterQuery<OfficeDocument>;

    if (args.address) {
      f.address = { $regex: args.address, $options: 'i' };
    }

    if (args.price) {
      const [gte, lte] = args.price ?? [];
      f.pricePerHour = {};

      if (gte) {
        f.pricePerHour.$gte = gte;
      }

      if (lte) {
        f.pricePerHour.$lte = lte;
      }
    }

    return f;
  }

  private buildOptions(args: OfficesOptionsType): QueryOptions {
    const o = {} as QueryOptions;

    if (args.skip) {
      o.skip = args.skip;
    }

    if (args.take) {
      o.limit = args.take;
    }

    if (args.sortBy) {
      const direction = args.sortDirection === SortDirection.ASC ? 1 : -1;

      o.sort = { [args.sortBy]: direction };
    }

    return o;
  }
}
