import { Injectable } from '@nestjs/common';

import { OfficesRepository } from './offices.repository';
import { GetOfficesArgs } from './dto';

@Injectable()
export class OfficesService {
  constructor(private readonly officesRepository: OfficesRepository) {}

  async getAll(args: GetOfficesArgs) {
    return this.officesRepository.findAll(args);
  }
}
