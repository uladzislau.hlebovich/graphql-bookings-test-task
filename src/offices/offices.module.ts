import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { OfficesService } from './offices.service';
import { OfficesResolver } from './offices.resolver';
import { OfficeModel, OfficeSchema } from './models';
import { OfficesRepository } from './offices.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: OfficeModel.name, schema: OfficeSchema },
    ]),
  ],
  providers: [OfficesResolver, OfficesService, OfficesRepository],
})
export class OfficesModule {}
