import { registerEnumType } from '@nestjs/graphql';

export enum OfficesSortField {
  PRICE = 'pricePerHour',
}

registerEnumType(OfficesSortField, {
  name: 'OfficesSortField',
});
