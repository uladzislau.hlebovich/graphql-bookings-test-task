import { ArgsType, Field, Int } from '@nestjs/graphql';
import { IsNumber, IsOptional, Max } from 'class-validator';

@ArgsType()
export class PaginationArgs {
  @Field(() => Int, { nullable: true, defaultValue: 10 })
  @Max(100)
  @IsOptional()
  take = 10;

  @Field(() => Int, { nullable: true, defaultValue: 0 })
  @IsNumber()
  @IsOptional()
  skip = 0;
}
