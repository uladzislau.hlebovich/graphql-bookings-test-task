import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { BookingModel } from './booking.model';
import { AccountModel } from '../../auth/models';

export type OfficeBookingDocument = BookingModel & Document;

@Schema({ collection: 'bookings' })
export class OfficeBookingModel {
  entityType!: string;

  account!: AccountModel;

  fromDate!: Date;

  toDate!: Date;
}

export const OfficeBookingSchema =
  SchemaFactory.createForClass(OfficeBookingModel);
