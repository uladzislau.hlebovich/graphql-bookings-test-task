import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { BookingsService } from './bookings.service';
import { BookingsResolver } from './bookings.resolver';
import { BookingsRepository } from './bookings.repository';
import {
  BookingModel,
  BookingSchema,
  CarBookingModel,
  CarBookingSchema,
  OfficeBookingModel,
  OfficeBookingSchema,
} from './models';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: BookingModel.name,
        schema: BookingSchema,
        discriminators: [
          { name: OfficeBookingModel.name, schema: OfficeBookingSchema },
          { name: CarBookingModel.name, schema: CarBookingSchema },
        ],
      },
    ]),
  ],
  providers: [BookingsResolver, BookingsService, BookingsRepository],
})
export class BookingsModule {}
