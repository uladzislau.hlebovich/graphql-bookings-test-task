import { Field, GraphQLISODateTime, InputType } from '@nestjs/graphql';

@InputType()
export class CreateOfficeBookingInput {
  @Field(() => String)
  id!: string;

  @Field(() => GraphQLISODateTime)
  fromDate!: Date;

  @Field(() => GraphQLISODateTime)
  toDate!: Date;
}
