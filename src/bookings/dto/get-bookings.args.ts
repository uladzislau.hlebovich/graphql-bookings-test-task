import { ArgsType, Field, GraphQLISODateTime } from '@nestjs/graphql';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsDate,
  IsOptional,
} from 'class-validator';

import { PaginationArgs } from '../../shared/dto';

@ArgsType()
export class getBookingsArgs extends PaginationArgs {
  @Field(() => [GraphQLISODateTime], { nullable: 'itemsAndList' })
  @IsDate({ each: true })
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsOptional()
  date?: [Date | null, Date | null];
}
